# Cookiecutter template for a python CLI program

## Usage

```
	pip install cookiecutter
	cookiecutter bb:princeton_genomics/cookiecutter-python-cli.git
```

You will be prompted for basic information which is used in the template.

## Project Structure

```
	.
	|-- README.md
	|-- LICENSE
	|-- install.sh                    <- Use `pip3 install -e .` to install in devmode
	|-- .gitignore
	|-- setup.py
	|-- {{cookicutter.project_name}}  <- Main package directory
	  |-- __init__.py                 <- Tell python this is a package
	  |-- __main__.py                 <- Entry point for the CLI
	  |-- classmodule.py
	  |-- funcmodule.py
```
