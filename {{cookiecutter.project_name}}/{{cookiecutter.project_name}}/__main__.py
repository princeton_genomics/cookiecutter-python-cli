#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" {{cookiecutter.project_name}}

{{cookiecutter.short_description}}
"""

import argparse
import logging
from .classmodule import MyClass
from .funcmodule import my_function

__version__ = "0.1"
__author__ = "{{cookiecutter.author_full_name}}"
__author_email__ = "{{cookiecutter.author_email}}"
__copyright__ = "Copyright {{cookiecutter.year}}, {{cookiecutter.author_full_name}}"
__license__ = "MIT https://opensource.org/licenses/MIT"


def main():

    parser = argparse.ArgumentParser(
        description="{{cookiecutter.short_description}}",
        epilog="As an alternative to the commandline, params can be placed in "
        "a file, one per line, and specified on the commandline like "
        "'%(prog)s @params.conf'.",
        fromfile_prefix_chars='@')
    # TODO Specify your real parameters here.
    parser.add_argument("argument",
                        help="pass ARG to the program",
                        metavar="ARG")
    parser.add_argument("-v", "--verbose",
                        help="increase output verbosity",
                        action="store_true")
    parser.add_argument("--version", action="version",
                        version="%(prog)s " + __version__)
    args = parser.parse_args()

    # Setup logging
    if args.verbose:
        loglevel = logging.DEBUG
    else:
        loglevel = logging.INFO

    logging.basicConfig(format="%(levelname)s: %(message)s", level=loglevel)

    # TODO Replace this with your actual code.
    print("Hello there.")
    logging.info("You passed an argument.")
    logging.debug("Your Argument: %s" % args.argument)

    my_function('hello world')

    my_object = MyClass('MyName')
    my_object.say_name()


# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()
