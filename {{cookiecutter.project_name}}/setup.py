#!/usr/bin/env python
# -*- coding: utf-8 -*-


from setuptools import setup

setup(
    name='{{cookiecutter.project_name}}',
    version='0.1.0',
    packages=['{{cookiecutter.project_name}}'],
    entry_points={
        'console_scripts': [
            '{{cookiecutter.project_name}}={{cookiecutter.project_name}}.__main__:main'
        ]
    })
